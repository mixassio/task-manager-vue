import arrayMove from "array-move";
import widgets from "./widgets";

export default {
  namespaced: true,
  state: {
    widgets,
    myWidgets: []
  },
  mutations: {
    logIn(state) {
      state.isAuth = true;
    },
    logOut(state) {
      state.isAuth = false;
    },
    addMyWidget(state, payload) {
      console.log("addMyWidget -> payload", payload);
      const widget = { ...payload, id: state.myWidgets.length };
      state.myWidgets.push(widget);
      console.log(state);
    },
    deleteMyWidget(state, { myWidgetId }) {
      state.myWidgets = state.myWidgets
        .filter(el => el.id !== myWidgetId)
        .map((el, ind) => ({ ...el, id: ind }));
    },
    moveCard(state, payload) {
      const { newIndex, oldIndex } = payload;
      console.log("moveCard -> newIndex, oldIndex", newIndex, oldIndex);
      state.myWidgets = arrayMove(state.myWidgets, oldIndex, newIndex).map(
        (el, ind) => ({ ...el, id: ind })
      );
      console.log(state);
    }
  },
  actions: {}
};
