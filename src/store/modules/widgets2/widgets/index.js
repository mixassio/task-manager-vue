import weather1 from "./weather1";
import weather2 from "./weather2";
import weather3 from "./weather3";
import forex from "./forex";
import currensy from "./currensy";

export default [weather1, weather2, weather3, currensy, forex].map(
  (el, idx) => ({ ...el, idWidget: idx })
);
