import Vue from "vue";
import Vuex from "vuex";
import widgets from "./modules/widgets";
import widgets2 from "./modules/widgets";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isAuth: false
  },
  mutations: {
    logIn(state) {
      state.isAuth = true;
    },
    logOut(state) {
      state.isAuth = false;
    }
  },
  actions: {},
  modules: {
    widgets,
    widgets2
  }
});
